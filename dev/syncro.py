
import os
from subprocess import Popen, PIPE
from os import listdir, path, mkdir, rename, system

# lyrics
links = [ ( "https://www.youtube.com/playlist?list=PLhD8GuXlNbtSfCBomEvtL0K5qBHKVz-h9", "lyrics"),
# rest
	  #( "https://www.youtube.com/playlist?list=PLhD8GuXlNbtT0MSn3GUzuoteGFgCAv5Hq", "rest"),
# gaelgie
	  #( "https://www.youtube.com/playlist?list=PLhD8GuXlNbtR-o_S8MZv_n2SU7w7iyy61", "gaelgie"),
# writing
#	  ( "https://www.youtube.com/playlist?list=PLnCckeb08Fuw1VCOLizU84Lf2zU_KUv2p", "writing"),
# עברי
	  ( "https://www.youtube.com/playlist?list=PLzPIJ2K41nzwUA_Isz0uqBHHJP31sTve_", "עברי"),
# doom
	#  ( "https://www.youtube.com/playlist?list=PLhD8GuXlNbtQ3Dkj21C7B2wK_HpZk5iPZ", "doom"),
#("https://www.youtube.com/watch?v=l3LFML_pxlY&list=PLhD8GuXlNbtRRXgOcMG5JsRyBdler3rcd", "sandg"),

#("https://www.youtube.com/watch?v=zN9n1fVhIjg&list=PLhD8GuXlNbtSawxHkV-M8SMbQLZl3Vgb_", "ATC")
	  ]





def downloader(playlist:str,links:str=None,down_opt='a',del_opt='y'):

    file_type = ".ytl"
    song_file_type = ".mp3"

    yt_prefix = "https://www.youtube.com/watch?v="

    download_format = "mp3"

    download_dir=""


    # if playlist wasn't initialized
    if playlist == "" or playlist is None:
        print("Missing playlist path!")  # missing playlist
        return 1  # exit

    # search for substring in entire string list
    def s_in_s_list(s, l):
        for i,v in enumerate(l):  # run on list
            if s in v:  # if substring
                return i  # found
        return -1  # not found


    # returns the song name by its hash and the playlist (if parse, parses the special chars); test:input(get_song_name_by_hash("6wDgMzYkrEU","ATC"))
    def get_song_name_by_hash(hsh, playlist, parse=False):
        if not playlist.endswith(file_type):  # if given playlist isnt a file name
            playlist += file_type  # add file type
        if not path.isfile(playlist):  # if playlist file isnt found
            return None  # return failed to find
        r = None  # init return var as not found
        with open(playlist, "r") as f:  # open playlist file
            s = f.read()  # read file
            if hsh in s:  # if hash in file
                r = s.split(hsh + "\n")[-1].split("\n")[
                    0
                ]  # split on hash\n, split the last part (pass the hash) by \n and get the first part
        return r if not parse else parse_song_name(r)  # return song name


    # something_hello-12345678912.mp3
    def get_hash(file):
        return file[-15:-4]  # get the hash of the file


    # returns stripped file name (without its type)
    def rm_type(f):
        return ".".join(f.split(".")[:-1])


    # parses a song name to remove special chars; test:# parse_song_name("Against+The+Current%3A+Runaway+%28LYRIC+VIDEO%29")
    def parse_song_name(name):
        n = name.split("%")  # split on special chars
        pairs = [
            ("%" + n[i + 1][:2], chr(int(n[i + 1][:2], 16))) for i in range(len(n) - 1)
        ]  # create a pair list of special char hash and the special char

        for pair in pairs:  # run on pair list
            name = name.replace(pair[0], pair[1])  # replace each pair

        name = name.replace("+", " ")  # replace + with whitespace

        return name  # return formatted name


    downloads = []

    removes = []

    playlist_file = ""  # init filestr
    if playlist.endswith(file_type):  # if as file
        playlist_file = playlist  # save fileStr
        playlist = rm_type(playlist)  # strip type and save at var
    else:  # if as playlist name
        playlist_file = playlist + file_type  # save fileStr as name.mp3

    s = None
    if links is None and path.isfile(playlist_file):  # if playlist is in WhiteList and the file exists
        with open(playlist_file, "r") as file:  # open the file
            s = file.read()
    elif not links is None:
        s = links
            
    s = s.split("\n")  # split on song seperator

    vids = s #[i[:11] for i in s]  # get hashes
    if "" in vids:  # if any empty strings
        vids.remove("")  # remove

    if not path.isdir(download_dir + playlist + "/"):  # if playlist directory doesnt exist
        print(
            "Playlist Directory Not Found!\nTrying to create playlist directory.."
        )  # print
        try:
            mkdir(download_dir + playlist + "/")  # try to create
        except OSError:  # failed to create dir
            print(
                "filed to create directory",
                download_dir + playlist + "/",
                "make sure folder has write privilages!",
            )  # inform user
            exit(1)  # exit with error code 1
        print("Created Playlist Directory.")  # success
    else:  # directory found
        print("Playlist Directory Found.")  # print

    files = listdir(download_dir + playlist + "/")  # get all files in the dir

    # input(files)
    # get vid hashes not in files list
    for hsh in vids:  # run on hashes
        if s_in_s_list(hsh, files) == -1:  # if hash not in file list
            downloads.append(
                (get_song_name_by_hash(hsh, playlist, True), hsh)
            )  # add (song_name, hash) to download list

    # get files not in vids list
    for sng in files:  # run on files in folder
        if (
            s_in_s_list(get_hash(sng), vids) == -1 and sng != ".dnld.sh"
        ):  # if file's hash isnt in hashes list and not .dnld.sh
            removes.append(sng)  # add filename to remove list


    # check for songs without hash in filename
    for pair in downloads:  # run on download pairs
        index = -1
        if (index := s_in_s_list(pair[0], removes)) != -1 and rm_type(removes[index]) == pair[0]:  # if song name in removes list
            print(pair[0])
            print(removes[index])
            print(rm_type(removes[index]) == pair[0])
            r = input(
                "Found file: "
                + pair[0]
                + "\n    in the removal list, and it appears in the download list as well.\n    Do you want to add the hash to the file name? [Y/n]: "
            )  # inform user and wait for input (N/Y)
            if r.lower() != "n":  # if user didnt input No (the default is Yes)
                rename(  # rename file
                    # From:
                    download_dir + playlist  #
                    + "/"  # playlist folder/
                    + pair[0]  # song name
                    + song_file_type,  # file type (.mp3)
                    # To:
                    download_dir + playlist  # playlist folder
                    + "/"  # /
                    + pair[0]  # song name
                    + "-"  # seperator
                    + pair[1]  # hash code
                    + song_file_type,  # file type (.mp3)
                )
                downloads.remove(pair)  # remove pair from downloads
                removes.remove(pair[0] + song_file_type)  # remove file from removes

    rem=[]
    for i,p in enumerate(downloads):
        print("Song name:",p[0])
        if down_opt == None:
            r = input("Do you want to download it? [Y=yes/n=no/a=Yes to all]: ").lower()
        else:
            r = down_opt
        if r == 'n':
            rem.append(p)
        elif r == 'a':
            break
    for r in rem:
        downloads.remove(r)

    rem = []
    for i,s in enumerate(removes):
        print("Song file name:",rm_type(s))
        if del_opt == None:
            r = input("Do you want to delete the song file? [y/N]: ").lower()
        else:
            r = del_opt
        if r != 'y':
            rem.append(s)
    for i in rem:
        removes.remove(i)

    print()
    if len(downloads) != 0:  # if any downloads
        print("Songs To Download:")  # print to user
        print("    -", "\n    - ".join([pair[0] for pair in downloads]), "\n")
    else:  # no downloads
        print("No Songs To Download.\n")  # inform

    if len(removes) != 0:  # if any
        print("Song Files To Remove: ")  # print to user
        print("    -", "\n    - ".join(removes), "\n")
    else:  # no removes
        print("No Songs To Remove.\n")  # inform


    playlist = download_dir + playlist


    l = "youtube-dl -x --audio-format "+download_format+" "+' '.join(['"'+yt_prefix+p[1]+'"' for p in downloads])
    #print(l)
    #print("\n\nyoutube-dl -x --audio-format mp3 "+l)
    if len(downloads) != 0:
        print("Downloading Mew Songs...")
        system("cd "+playlist+"/;"+l)

    if len(removes) != 0:
        print("\n\nRemoving Old Songs...")
        system("cd "+playlist+"/;rm \""+"\" \"".join(removes) + "\"")


for i in links:
    #print(i)
    print("Syncing playlist",i[-1],"(May take a while)")
    
    p = Popen(['youtube-dl','--get-id',i[0]],stdout=PIPE)
    r = p.communicate(0)

    # print(r)

    links = r[0].decode()
    if links == '':
        print("ERROR: No links were received for link:",i[0],"(playlist name:"+i[-1]+')')
        continue

    # print("playlist:",i[-1],"\nLinks:",links.replace("\n",'  '))
    # input()

    downloader(i[-1],links,True)
